class SendTicketReplyMailJob < ApplicationJob
  queue_as :default

  def perform(reply)
    UserMailer.ticket_reply(reply).deliver_later
  end
end
