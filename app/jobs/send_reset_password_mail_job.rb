class SendResetPasswordMailJob < ApplicationJob
  queue_as :default

  def perform(user, subdomain)
    UserMailer.password_reset(user, subdomain).deliver_later
  end
end
