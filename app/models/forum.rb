class Forum < ApplicationRecord
  belongs_to :campaign
  has_many :products, dependent: :destroy
end
