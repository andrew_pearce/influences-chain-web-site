class CampaignInfluencer < ApplicationRecord
  acts_as_paranoid

  enum state: %w(invited approved rejected suspended)

  belongs_to :influencer, class_name: User, foreign_key: :user_id
  belongs_to :campaign
  has_many :tickets

  after_create :send_registration_email
  after_update :create_ticket, if: :contract_changed?
  after_update :new_state_notification, if: :state_changed?

  mount_base64_uploader :contract, CampaignUploader

  private

  def send_registration_email
    if influencer.password_hash.present?
      UserMailer.campaign_invitation(influencer, campaign).deliver!
    else
      influencer.generate_password_token
      UserMailer.influencer_registration(influencer, campaign).deliver!
    end
  end

  def create_ticket
    tickets.create(email: influencer.email, text: I18n.t('.tickets.contract'))
  end

  def new_state_notification
    UserMailer.new_state(self, campaign.tenant.subdomain).deliver!
  end
end
