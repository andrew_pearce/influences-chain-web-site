class SocialNetwork < ApplicationRecord
  enum kind: %w(facebook twitter snapchat instagram)

  belongs_to :influencer, class_name: User, foreign_key: :user_id
end
