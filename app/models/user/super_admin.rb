class User::SuperAdmin < User
  def superadmin?
    true
  end

  def influencer?
    false
  end
end
