class User::TenantContact < User
  after_create :send_registration_for_contact

  belongs_to :parent, foreign_key: :parent_id, class_name: Tenant

  def campaign_admin?
    false
  end

  def influencer?
    false
  end

  def tenant_contact?
    true
  end

  def after_password_set_url
    Rails.application.routes.url_helpers.new_sessions_path(subdomain: parent&.name)
  end
end
