class Category < ApplicationRecord
  belongs_to :user
  belongs_to :product
  has_many :topics, dependent: :destroy

  mount_base64_uploader :logo, LogoUploader
end
