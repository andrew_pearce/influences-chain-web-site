class Ticket < ApplicationRecord
  belongs_to :tenant
  belongs_to :campaign_influencer
  has_many :replies

  after_create :notify_tenant_owner

  private

  def notify_tenant_owner
    campaign_influencer ? UserMailer.admin_ticket_notification(campaign_influencer.campaign.owner, self).deliver! : notify_admins
  end

  def notify_admins
    User::CampaignAdmin.find_each do |admin|
      UserMailer.admin_ticket_notification(admin, self).deliver!
    end
  end
end
