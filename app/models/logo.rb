class Logo < ApplicationRecord
  enum placement: ['Top Left', 'Top Center', 'Top Right', 'Full Size']

  belongs_to :campaign, optional: true

  mount_uploader :image, LogoUploader
end
