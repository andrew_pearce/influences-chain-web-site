class Tenant < ApplicationRecord
  acts_as_paranoid

  EXPORT_ATTRIBUTES = %w(name client_list_available full_size_logo_url use_logo logo_url).freeze

  belongs_to :owner, class_name: User, foreign_key: :user_id
  has_many :contacts, foreign_key: :parent_id, class_name: 'User::TenantContact', dependent: :destroy
  has_many :campaigns, dependent: :destroy
  has_many :active_campaigns, -> { where('end_date >= ?', Date.current) }, class_name: Campaign
  has_many :completed_campaigns, -> { where('end_date < ?', Date.current) }, class_name: Campaign
  has_many :tickets, dependent: :destroy

  before_save :name_format

  mount_base64_uploader :logo, TenantLogoUploader
  mount_base64_uploader :full_size_logo, TenantLogoUploader

  def subdomain
    name&.downcase&.gsub(' ', '-')
  end

  private

  def name_format
    name&.squish!
  end
end
