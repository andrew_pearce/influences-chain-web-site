module Password
  extend ActiveSupport::Concern
  included do
    after_create :send_password_request

    def send_password_request
      return unless campaign_admin?
      generate_password_token
      UserMailer.send_password_request(self).deliver!
    end

    def generate_password_token
      self.password_token = SecureRandom.urlsafe_base64
      self.password_token_expires_at = Time.now + 1.day
      save
    end
  end
end
