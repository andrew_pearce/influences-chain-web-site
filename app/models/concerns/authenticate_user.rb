module AuthenticateUser
  extend ActiveSupport::Concern
  included do
    attr_accessor :password

    before_save :encrypt_password

    def self.authenticate(email, password)
      user = find_by("lower(email) = ?", email.downcase)
      user&.password_hash == BCrypt::Engine.hash_secret(password, user&.password_salt) ? user : nil
    rescue
      nil
    end

    private

    def encrypt_password
      if password.present?
        self.password_salt = BCrypt::Engine.generate_salt
        self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      end
    end
  end
end
