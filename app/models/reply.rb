class Reply < ApplicationRecord
  belongs_to :user
  belongs_to :ticket

  after_create :send_mail_reply

  def send_mail_reply
    SendTicketReplyMailJob.set(wait: 3.seconds).perform_later(self)
  end
end
