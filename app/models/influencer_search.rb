class InfluencerSearch
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_accessor :first_name, :surname, :email, :matrimonial_surname, :gender,
                :marital_status, :profession, :personality_type, :birth_year,
                :childrens, :key_influencer, :friends_and_family, :first_address,
                :second_address, :third_address, :city, :postal_code, :interior,
                :country, :username
end
