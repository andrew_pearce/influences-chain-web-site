module CampaignsHelper
  def tenants_options
    Tenant.all.map { |t| [t.name, t.id] }
  end

  def placement_class(placement)
    placement&.downcase&.gsub(' ', '-')
  end

  def campaign_bg
    campaign ? campaign.logos&.where(placement: 'Full Size')&.last&.image&.url : tenant&.full_size_logo&.url
  end
end
