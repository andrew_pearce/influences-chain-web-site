module TaskUsersHelper
  def task_completed?(task)
    task.users.include?(current_tenant_user)
  end
end
