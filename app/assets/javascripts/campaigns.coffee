$(document).on 'turbolinks:load', ->
  $('#campaign_limit').attr('disabled', $('#campaign_unlimited').is(':checked'))
  $('#campaign_unlimited').on 'change', ->
    $('#campaign_limit').attr('disabled', $(@).is(':checked'))

$(document).on 'click', '#select_all_influencers', ->
  $('.influencer-checkbox').prop('checked', $(@).prop('checked'))
