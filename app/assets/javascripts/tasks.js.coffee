$(document).on 'turbolinks:load', ->
  toggleSurveyLink($('#task_task_type'))
  $('#task_task_type').on 'change', ->
    toggleSurveyLink($(@))

toggleSurveyLink = (el)->
  if el.val() == 'survey' then $('#survey-link').removeClass('d-none') else $('#survey-link').addClass('d-none')
