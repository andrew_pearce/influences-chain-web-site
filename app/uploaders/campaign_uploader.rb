class CampaignUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def public_id
    self.file.original_filename + SecureRandom.base64
  end
end
