$('#topic_modal button.close').click()
$('#topic_modal form')[0].reset()
$('.topics').append("<%= j(render 'row', topic: topic) %>")
$('html, body').animate
  scrollTop: $("[data-topic-id=#{<%= topic.id %>}]").offset().top
