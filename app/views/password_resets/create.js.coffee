if $('#password-reset-modal').length
  $('#password-reset-modal').replaceWith("<%= j render 'shared/password_reset_modal', user: user %>")
else
  $('body').append("<%= j render 'shared/password_reset_modal', user: user %>")
$('#password-reset-modal').modal('show')
