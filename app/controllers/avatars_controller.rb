class AvatarsController < ApplicationController
  expose(:user)

  def update
    user.update(avatar_params)
    redirect_to :back
  end

  private

  def avatar_params
    params.require(:user).permit(:avatar)
  end
end
