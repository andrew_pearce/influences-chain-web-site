class ForumsController < ApplicationController
  before_action :require_tenant_login

  expose(:forum) { campaign.forum }
  expose(:all_products) { forum.products }

  layout 'tenant'
end
