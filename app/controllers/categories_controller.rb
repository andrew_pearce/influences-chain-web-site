class CategoriesController < ApplicationController
  before_action :require_tenant_login

  expose(:category)
  expose(:product)
  expose(:forum) { product.forum }
  expose(:campaign) { forum.campaign }
  expose(:all_topics) { category.topics }

  layout 'tenant'

  def create
    head :no_content unless category.update(category_params)
  end

  def destroy
    category.destroy
  end

  private

  def category_params
    params.require(:category).permit(:title, :description, :user_id, :logo)
                             .merge(product_id: params[:product_id])
  end
end
