class PostsController < ApplicationController
  before_action :require_tenant_login

  expose(:post)
  expose(:topic)

  layout 'tenant'

  def create
    head :no_content unless !topic.closed && post.update(post_params)
  end

  def destroy
    post.destroy
  end

  private

  def post_params
    params.require(:post).permit(:text, :user_id).merge(topic_id: params[:topic_id])
  end
end
