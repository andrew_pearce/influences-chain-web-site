class TasksController < ApplicationController
  layout 'tenant'

  expose(:campaign)
  expose(:tasks) { campaign.tasks.by_type(current_tenant_user, params[:type]) }
end
