class TenantsController < ApplicationController
  before_action :require_tenant_login

  expose(:active_campaigns) { tenant.active_campaigns }
  expose(:influencer_campaigns) { current_tenant_user.connected_campaigns }
  expose(:completed_campaigns) { tenant.completed_campaigns }
  expose(:contact_campaign) { current_tenant_user.parent }

  layout 'tenant'
end
