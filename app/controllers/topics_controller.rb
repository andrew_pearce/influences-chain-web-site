class TopicsController < ApplicationController
  before_action :require_tenant_login
  before_action :close_topic, if: proc { params[:closed] }, only: :show
  before_action :add_reffered, if: proc { params[:topic][:referred] }, only: :update

  expose(:topic)
  expose(:category)
  expose(:product) { category.product }
  expose(:forum) { product.forum }
  expose(:campaign) { forum.campaign }
  expose(:posts) { topic.posts }

  layout 'tenant'

  def create
    head :no_content unless topic.update(topic_params)
  end

  def update
    redirect_to category_topic_path(category, topic)
  end

  def destroy
    topic.destroy
  end

  private

  def close_topic
    topic.update(closed: params[:closed])
  end

  def add_reffered
    topic.update(referred: referred) if referred
  end

  def referred
    @referred ||= Topic.find_by(subject: params[:topic][:referred])
  end

  def topic_params
    params.require(:topic).permit(:subject, :user_id).merge(category_id: params[:category_id])
  end
end
