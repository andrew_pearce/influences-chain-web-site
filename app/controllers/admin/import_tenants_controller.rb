require 'tenant_import'

class Admin::ImportTenantsController < ApplicationController
  expose(:tenant) { Tenant.find_by(id: params[:tenant][:tenant_id]) }

  def create
    TenantImport.new(params['tenant']['file']).import
    redirect_to root_path
  end
end
