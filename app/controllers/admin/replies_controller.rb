class Admin::RepliesController < ApplicationController
  expose(:reply)

  def create
    head no_content unless reply.update(reply_params)
  end

  private

  def reply_params
    params.require(:reply).permit(:text, :user_id).merge(ticket_id: params[:ticket_id])
  end
end
