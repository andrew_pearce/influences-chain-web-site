class Admin::TicketsController < AdminsController
  before_action :close_ticket, if: proc { params[:closed] }, only: :update

  expose(:tickets) { Ticket.all.order(id: :desc) }
  expose(:ticket)

  def update
    redirect_to admin_tickets_path
  end

  private

  def close_ticket
    ticket.update(closed: params[:closed])
  end
end
