class Admin::ContactsController < AdminsController
  expose(:parent) { params[:type].camelize.constantize.find(params[:parent_id]) }
  expose(:contacts) { parent.contacts }
  expose(:contact, scope: ->{ contacts })

  def create
    if contact.update(contact_params)
      redirect_to path
    else
      flash[:error] = contact.errors.messages
      redirect_to :back
    end
  end

  def destroy
    contact.destroy
    redirect_to edit_admin_tenant_path(parent)
  end

  private

  def contact_params
    params.require("user_#{params[:type]}_contact").permit!
  end

  def path
    params[:type] == 'campaign' ? edit_admin_campaign_path(parent, tenant: parent.tenant&.id) : edit_admin_tenant_path(parent)
  end
end
