class Admin::InfluencersController < AdminsController
  expose :influencers, ->{ User::Influencer.all }
  expose :influencer, model: User::Influencer, build: ->(influencer_params, scope){ scope.new }

  def update
    create
  end

  def create
    if influencer.update(influencer_params)
      redirect_to admin_influencers_path
    else
      flash[:error] = influencer.errors.messages
      redirect_to :back
    end
  end

  def destroy
    influencer.destroy
    redirect_to admin_influencers_path
  end

  private

  def influencer_params
    params.require(:user_influencer).permit(
      :username, :first_name, :surname, :matrimonial_surname, :primary_language,
      :email, :avatar, :phone_number, :phone_type, :gender, :birth_year,
      :marital_status, :childrens, :profession, :personality_type, :key_influencer,
      :friends_and_family, :first_address, :second_address, :third_address,
      :country, :city, :postal_code, :interior,
      social_networks_attributes: [:id, :kind, :url, :_destroy]
    )
  end
end
