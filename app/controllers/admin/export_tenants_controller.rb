require 'tenant_export'

class Admin::ExportTenantsController < ApplicationController
  expose(:tenant) { Tenant.find_by(id: params[:tenant][:tenant_id]) }

  def create
    file = TenantExport.new(Tenant.first).result
    respond_to do |format|
      format.json do
        send_data file.to_json, filename: "tenant_#{tenant.name}.json"
      end
    end
  end
end
