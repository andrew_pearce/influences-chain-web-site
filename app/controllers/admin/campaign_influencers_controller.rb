class Admin::CampaignInfluencersController < AdminsController
  expose(:campaign)
  expose(:campaign_influencers) { campaign.campaign_influencers }
  expose(:campaign_influencer)
  expose(:influencers) { campaign.influencers }
  expose(:ids) { params.dig(:influencers, :ids) }
  expose(:users) { User::Influencer.where(id: ids) }

  def create
    campaign.influencers << (users - influencers)
    redirect_to admin_campaign_campaign_influencers_path(campaign)
  end

  def update
    campaign_influencer.update(state: params[:state].to_i)
  end

  def destroy
    campaign_influencer.destroy
    redirect_to admin_campaign_campaign_influencers_path(campaign)
  end
end
