class Admin::CampaignsController < AdminsController
  expose :campaign, build: ->(campaign_params, scope){ scope.new }
  expose(:tenant) { Tenant.where(id: campaign&.tenant_id).last }
  expose(:contacts) { campaign.contacts }
  expose(:campaigns) { Campaign.includes(:tenant).all }

  def update
    create
  end

  def create
    if campaign.update(campaign_params)
      redirect_to tenant ? edit_admin_tenant_path(tenant) : admin_tenants_path
    else
      flash[:error] = campaign.errors.messages
      redirect_to :back
    end
  end

  def destroy
    campaign.destroy
    redirect_to edit_admin_tenant_path(tenant)
  end

  private

  def campaign_params
    params.require(:campaign).permit(:id, :project_name, :start_date, :end_date, :tenant_id,
                                     :start_auto, :end_auto, :location, :use_tenant_logo,
                                     :tenant_logo_placement, :terms, :contract, :welcome_text,
                                     :fulfilment_email, :fulfilment_text, :unlimited, :limit,
                                     :user_id, :use_forum,
                                     logos_attributes: [:id, :image, :placement, :_destroy],
                                     forum_attributes: [:id, :title, :_destroy])
  end
end
