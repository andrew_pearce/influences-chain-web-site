class PasswordChangesController < ApplicationController
  before_action :check_passwords_eq, only: %w(update create)
  before_action :check_token, only: %w(new create)

  expose(:user) { User.find_by(password_token: params[:token]) }

  def update
    current_user.change_password(params)
    redirect_to root_path
  end

  def create
    user&.change_password(params)
    redirect_to user&.after_password_set_url || new_admin_session_path(subdomain: :siteadmin)
  end

  private

  def check_token
    redirect_to root_path if params[:token].blank? || (user&.password_token_expires_at || DateTime.new(0)) < DateTime.now
  end
end
