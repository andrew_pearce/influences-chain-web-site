class TaskUsersController < ApplicationController
  expose(:campaign)
  expose(:task)
  expose(:tasks) { campaign.tasks.by_type(current_tenant_user, params[:page_type]) }

  def create
    task_user_params['completed'] == '1' ? task.users << current_tenant_user : task.users.delete(current_tenant_user)
  end

  private

  def task_user_params
    params.require(:task_user).permit(:completed)
  end
end
