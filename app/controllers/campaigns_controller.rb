class CampaignsController < ApplicationController
  before_action :require_tenant_login

  expose(:campaign)

  layout 'tenant'
end
