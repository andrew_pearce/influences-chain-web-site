class SessionsController < ApplicationController
  expose(:new_session) { Session.new(session_params) }
  expose(:user) { User.authenticate(session_params.dig(:email), session_params.dig(:password)) }

  layout 'tenant'

  def create
    if user
      session["#{tenant.name}_user_id"] = user.id
      user.touch(:last_login_at)
    else
      flash[:error] = 'Email or password is wrong'
    end
    redirect_to root_path
  end

  def destroy
    session.delete("#{tenant.name}_user_id")
    redirect_to root_path
  end

  private

  def session_params
    params.require(:session).permit! if params[:session]
  end
end
