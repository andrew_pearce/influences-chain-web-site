class TenantImport
  def initialize(file)
    file = File.read(file.tempfile)
    @data = JSON.parse file
  end

  def import
    @tenant = create_tenant
    return unless @tenant
    create_users
    create_tenant_contacts
    create_campaigns
  end

  private

  def create_tenant
    tenant_attr = JSON.parse @data['tenant']['tenant']
    attrs = except_keys(tenant_attr)
    attrs['remote_logo_url'] = tenant_attr['logo']['url']
    attrs['remote_full_size_logo_url'] = tenant_attr['full_size_logo']['url']
    tenant = Tenant.find_by(name: attrs['name'])
    tenant&.really_destroy!
    Tenant.create(attrs)
  end

  def create_users
    @data['users'].each do |user|
      social_networks = user['social_networks']
      type = user['type']
      user = user['user']
      attrs = except_keys(user)
      next if User.find_by(email: attrs['email'], type: type)
      attrs['type'] = type
      attrs['remote_avatar_url'] = user['avatar']['url']
      u = User.create(attrs)
      social_networks.each do |network|
        attrs = except_keys(network)
        u.social_networks.create(network)
      end
    end
  end

  def create_tenant_contacts
    contacts = JSON.parse(@data['tenant']['contacts'])
    contacts.each do |contact|
      attrs = except_keys(contact)
      attrs['remote_avatar_url'] = contact['avatar']['url']
      attrs['parent_id'] = @tenant.id
      User::TenantContact.create(attrs)
    end
  end

  def create_campaigns
    campaigns.each do |campaign_original|
      c = JSON.parse campaign_original['campaign']
      c_attr = except_keys(c)
      c_attr['remote_contract_url'] = c['contract']['url']
      c_attr['tenant_id'] = @tenant.id
      campaign = Campaign.create(c_attr)

      campaign_original['contacts'].each do |c|
        contact_attrs = JSON.parse(c)
        contact_attrs = except_keys(contact_attrs)
        contact_attrs['parent_id'] = campaign.id
        User::CampaignContact.create(contact_attrs)
      end

      campaign_original['logos'].each do |l|
        l = JSON.parse(l)
        logo_attrs = except_keys(l)
        logo_attrs['campaign_id'] = campaign.id
        logo_attrs['remote_image_url'] = l['image']['url']
        Logo.create(logo_attrs)
      end

      campaign_original['tasks'].each do |task|
        t = JSON.parse(task)
        task_attrs = except_keys(t)
        task_attrs['campaign_id'] = campaign.id
        Task.create(task_attrs)
      end

      campaign_original['campaign_influencers'][0]['influencers'].each do |influencer|
        user = User::Influencer.find_by(email: influencer['influencer']['email'])
        CampaignInfluencer.create(campaign: campaign, influencer: user, state: :approved) if user
      end

      create_forum(campaign, campaign_original)
    end
  end

  def create_forum(campaign, campaign_attrs)
    title = JSON.parse(campaign_attrs['forum']['forum'])['title']
    forum = campaign.create_forum(title: title)

    campaign_attrs['forum']['products'].each do |product|
      product_attrs = JSON.parse(product['product'])
      product_attrs = except_keys(product_attrs)
      product_attrs['user_id'] = user(product)&.id
      new_product = forum.products.create(product_attrs)

      product['categories'].each do |category|
        category_attrs = JSON.parse(category['category'])
        attrs = except_keys(category_attrs)
        attrs['remote_logo_url'] = category_attrs['logo']['url']
        attrs['user_id'] = user(category)&.id
        new_category = new_product.categories.create(attrs)

        category['topics'].each do |topic|
          topic_attrs = JSON.parse(topic['topic'])
          topic_attrs = except_keys(topic_attrs)
          topic_attrs['user_id'] = user(topic)&.id
          new_topic = new_category.topics.create(topic_attrs)

          topic['posts'].each do |post|
            post_attrs = except_keys(JSON.parse(post['post']))
            post_attrs['user_id'] = user(post)&.id
            new_topic.posts.create(post_attrs)
          end
        end
      end
    end
  end

  def except_keys(hash)
    hash.except('id', 'created_at', 'updated_at', 'deleted_at', 'logo', 'full_size_logo', 'user_id', 'avatar', 'tenant_id', 'contract', 'image')
  end

  def user(hash)
    User.find_by(type: hash['user_type'], email: hash['user_email'])
  end

  def campaigns
    @data['tenant']['tenant_associations']['campaigns']
  end
end
