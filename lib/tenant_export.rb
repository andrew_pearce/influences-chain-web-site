class TenantExport
  def initialize(tenant)
    @tenant = tenant
  end

  def result
    {
      tenant: {
        tenant: @tenant.to_json,
        contacts: @tenant.contacts.to_json,
        tenant_associations: {
          campaigns: @tenant.campaigns.map do |c|
            { campaign: c.to_json,
              contacts: c.contacts.map do |contact|
                contact.to_json
              end,
              logos: c.logos.map do |logo|
                logo.to_json
              end,
              tasks: c.tasks.map do |task|
                task.to_json
              end,
              forum: {
                forum: c.forum.to_json,
                products: c.forum.products.map do |product|
                  {
                    product: product.to_json,
                    user_email: product.user&.email,
                    user_type: product.user&.type,
                    categories: product.categories.map do |category|
                      {
                        category: category.to_json,
                        user_email: category.user&.email,
                        user_type: category.user&.type,
                        topics: category.topics.map do |topic|
                          {
                            topic: topic.to_json,
                            user_email: category.user&.email,
                            user_type: category.user&.type,
                            posts: topic.posts.map do |post|
                              {
                                post: post.to_json,
                                user_email: category.user&.email,
                                user_type: category.user&.type
                              }
                            end
                          }
                        end
                      }
                    end
                  }
                end
              },
              campaign_influencers: c.campaign_influencers.map do |c_i|
                {
                  influencers: c.influencers.map do |influencer|
                    {
                      influencer: influencer
                    }
                  end
                }
              end
            }
          end
        }
      },
      users: users
    }
  end

  private

  def users_ids
    [@tenant.user_id, @tenant.campaigns.pluck(:user_id), @tenant.campaigns.map { |campaign| [campaign&.tasks&.pluck(:user_id), campaign&.forum&.products&.map { |product| [product.user_id, product.categories.map { |category| [category.user_id, category.topics.map { |topic| [topic.user_id, topic.posts.pluck(:user_id)] }] }] }, campaign.influencers.ids] }].flatten.compact.uniq
  end

  def users
    User.where(id: users_ids).map do |user|
      {
        user: user,
        type: user.type,
        social_networks: user.social_networks
      }
    end
  end
end
