require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module InfluencesChainWebSite
  class Application < Rails::Application
    Rails.application.secrets.each { |key, value| ENV[key.to_s] ||= value }
  end
end
