class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :surname
      t.string :matrimonial_surname
      t.string :title
      t.integer :language
      t.string :email
      t.integer :tenant_id

      t.timestamps
    end
  end
end
