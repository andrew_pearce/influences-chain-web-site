class AddPasswordTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :password_token, :string
    add_column :users, :password_token_expires_at, :datetime
  end
end
