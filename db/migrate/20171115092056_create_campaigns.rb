class CreateCampaigns < ActiveRecord::Migration[5.0]
  def change
    create_table :campaigns do |t|
      t.string :project_name
      t.date :start_date
      t.date :end_date
      t.integer :tenant_id
      t.boolean :start_auto
      t.boolean :end_auto
      t.integer :location
      t.boolean :use_tenant_logo
      t.string :tenant_logo_placement
      t.text :terms
      t.string :contract
      t.text :welcome_text
      t.string :fulfilment_email
      t.text :fulfilment_text
      t.boolean :unlimited
      t.integer :limit
      t.integer :user_id

      t.timestamps
    end
  end
end
