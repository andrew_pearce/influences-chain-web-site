class AddFullSizeLogo < ActiveRecord::Migration[5.0]
  def change
    add_column :tenants, :full_size_logo, :string
  end
end
