class CreateTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :topics do |t|
      t.string :subject
      t.references :category, foreign_key: true
      t.boolean :closed, default: false
      t.integer :referred_id
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
