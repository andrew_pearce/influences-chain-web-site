class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password_hash
      t.string :password_salt
      t.datetime :last_login_at
      t.string :first_name
      t.string :surname
      t.string :matrimonial_surname
      t.integer :primary_language
      t.string :email
      t.string :avatar
      t.string :address
      t.string :phone_number
      t.integer :phone_type
      t.integer :gender
      t.integer :birth_year
      t.integer :marital_status
      t.integer :childrens
      t.string :profession
      t.boolean :key_influencer
      t.boolean :friends_and_family
      t.text :social_accouns
      t.string :type

      t.timestamps
    end
  end
end
