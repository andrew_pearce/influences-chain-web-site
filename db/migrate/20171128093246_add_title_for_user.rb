class AddTitleForUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :title, :string
    add_column :users, :parent_id, :integer
  end
end
