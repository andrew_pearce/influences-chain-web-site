class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :title
      t.text :description
      t.references :product, foreign_key: true
      t.string :logo
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
