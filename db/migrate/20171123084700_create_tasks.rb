class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :summary
      t.text :description
      t.integer :user_id
      t.integer :task_type
      t.integer :campaign_id
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
