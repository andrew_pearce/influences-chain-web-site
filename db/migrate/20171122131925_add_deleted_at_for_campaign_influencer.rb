class AddDeletedAtForCampaignInfluencer < ActiveRecord::Migration[5.0]
  def change
    add_column :campaign_influencers, :deleted_at, :datetime
  end
end
