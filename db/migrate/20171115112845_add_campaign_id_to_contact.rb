class AddCampaignIdToContact < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :parent_id, :integer
    add_column :contacts, :type, :string
    remove_column :contacts, :tenant_id, :integer
  end
end
