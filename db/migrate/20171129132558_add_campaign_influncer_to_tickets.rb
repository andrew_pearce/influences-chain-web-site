class AddCampaignInfluncerToTickets < ActiveRecord::Migration[5.0]
  def change
    add_reference :tickets, :campaign_influencer, foreign_key: true
  end
end
