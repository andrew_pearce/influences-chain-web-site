class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :email
      t.text :text
      t.boolean :closed, default: false
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
