class AddUseTenantLogo < ActiveRecord::Migration[5.0]
  def change
    add_column :tenants, :use_logo, :boolean, default: true
  end
end
