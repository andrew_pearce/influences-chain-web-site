class ChangeSurveyField < ActiveRecord::Migration[5.0]
  def change
    rename_column :tasks, :survey_link, :survey
    change_column :tasks, :survey, :text
  end
end
