class AddAddressToUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :address, :first_address
    add_column :users, :second_address, :string
    add_column :users, :third_address, :string
    add_column :users, :country, :integer
    add_column :users, :city, :string
    add_column :users, :postal_code, :integer
    add_column :users, :interior, :string
  end
end
