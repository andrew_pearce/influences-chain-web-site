class AddContractToCampaignInfluencers < ActiveRecord::Migration[5.0]
  def change
    add_column :campaign_influencers, :contract, :string
  end
end
