class AddSurveyLink < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :survey_link, :string
    add_column :tasks, :due, :datetime
  end
end
