class CreateLogos < ActiveRecord::Migration[5.0]
  def change
    change_column :campaigns, :tenant_logo_placement, 'integer USING CAST(tenant_logo_placement AS integer)'

    create_table :logos do |t|
      t.string :image
      t.integer :placement
      t.integer :campaign_id

      t.timestamps
    end
  end
end
